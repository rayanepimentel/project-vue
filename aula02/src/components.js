import Vue from 'vue';

Vue.component('ButtonString', {
    template: ' <button @click="click">Click string {{ count }} times</button>',
    data() {
        return {
            count: 0,
        };
    },
    methods: {
        click() {
            this.count++
        },
    },
})

Vue.component('ButtonTemplateString', {
    template: `
    <button @click="click">
      Click templateString {{ count }} times
    </button>`,
    data() {
        return {
            count: 0,
        };
    },
    methods: {
        click() {
            this.count++
        },
    },
})

Vue.component('ButtonXTemplate', {
    template: '#button-x-template',
    data() {
        return {
            count: 0,
        };
    },
    methods: {
        click() {
            this.count++
        },
    },
})

Vue.component('ButtonInline', {
    data() {
        return {
            count: 0,
        };
    },
    methods: {
        click() {
            this.count++
        },
    },
})