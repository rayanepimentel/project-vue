## Componentes

1. <code>template</code> onde ficará basicamente nosso HTML
2. <code>script</code>onde ficará import e export 
3. <code>style</code>onde ficará a estilização
### Formas de criar um componente

1. Criando manualmente no src <code>nomeComponent.vue</code> dentro do escopo do App.vue
2. Criando no escopo global.
Acessando no App.vue, somente referenciando o nome do componente. 
- String 
```html
  template: ' <button @click="click">Click string {{ count }} times</button>',
```
- TemplateString 
```html
template: `
    <button @click="click">
      Click templateString {{ count }} times
    </button>`,
```
- XTemplate
- inline-template