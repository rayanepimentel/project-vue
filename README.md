# Project Vue
| Curso realizado pela [Digital Innovation One](https://web.digitalinnovation.one/course/desenvolvendo-sua-primeira-aplicacao-com-vuejs/learning/89a47cdb-7960-4274-9670-44f1bff8b997/?back=/browse)
## O que é Vue.js

### Requisitos básicos
- HTML/CSS/Jvascript
- Node.js (NPM/Yarn)
### Objetivos
- Aprender Vue.js => Módulo básico. Na biblioteca em 
- Conhecer conceitos básicos
- Construir um projeto prático
### O que vamos aprender
- Iniciar uma aplicação Vue.js
- Estrutura de um componente
- Controle e compartilhamento de estados
- Diretivas e Eventos => são conceitos do Angular
- Estilização dinâmica
- Ciclo de vida 
- Projeto prático
### O que é Vue.js
- Framework progressivo => Você consegue usa-lo em conjuntos com outros frameworks e bibliotecas, até aplicações single page complexas, de grande porte.
- Criado por Evan You => Pegar o que angular oferia de legal, tirar toda complexidade que o angular tem e que tivesse performace alta.
- Renderização declarativa => vai dizer o que quer que aconteça e o componente vai ser renderizado de acordo com o que foi declarou.
- Composição de componentes 
- Nuxt.js/Grisdsome/VuePress => PWA, Google Analytics, gerar mapa do site. O VuePress muito usado para documentação.
- NativeScript/Quasar/Vue Native => Desenvolvimento hibrido 
### Conhecendo a documentação
- vuejs.org
- github.com/vuejs/vue
### Configurando o VSCode(extensões)
- Vetur
- Vue 3 Snippets
- Auto Close Tag
- Settings Sync

### Vue CLI 
- cli.vuejs.org/
- Ferramenta principal para desenvolver em Vue
- sistema de plugin muito forte
- Não precisa injetar
- Interface gráfica, pode usar projeto usando uma UI
- Protipação instantanea 
- Preparado para futuro => já vem embutido

#### Como instalar Vue CLI

```javascript
npm install -g @vue/cli
# OR
yarn global add @vue/cli

```

- No terminal: 
1. windows: <code>npm install -g @vue/cli</code>
2. linux: <code>sudo npm install -g @vue/cli</code>
- <code>vue --version</code>

```javascript
vue create my-project
# OR
vue ui
```

### Vue CDN

No arquivo.html inclua:

```html
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
```

Testando:

No console do navegador digite Vue
```javascript
Vue
//function wn(e)
```
